<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'type_ticket',
        'is_insurance'
    ];

    function places()
    {
        return $this->belongsToMany(Place::class, 'tour_place', 'tour_id','place_id');
    }
    public $guarded=[];

}
