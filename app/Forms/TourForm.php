<?php

namespace App\Forms;

use App\Models\Place;
use Kris\LaravelFormBuilder\Form;

class TourForm extends Form
{
    public function buildForm()
    {
        $places=Place::all();
        $places_choices=[];
        foreach($places as $place){

            $places_choices[$place->id] = $place->indent_text . ' ' . $place->name;

        }
        if($this->getModel()&&$this->getModel()->id) {
            $method ='PUT';
            $url=route('tour.update',$this->getModel()->id);
        }else{
            $method='POST';
            $url=route('tour.store');
        }
        $this->formOptions=[
            'method'=>$method,
            'url'=>$url
        ];

        $this
            ->add('description', 'textarea', [
                'label' => 'descripción del tour',
                'rules' => 'required'
            ])
            ->add('places', 'choice', [

                'choices' =>$places_choices,
                'choice_options' => [
                    'wrapper' => ['class' => 'choice-wrapper'],
                    'label_attr' => ['class' => 'label-class'],
                ],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('is_insurance', 'choice', [

                'choices' =>['1' => 'yes', '0' => 'no'],
                'choice_options' => [
                    'wrapper' => ['class' => 'choice-wrapper'],
                    'label_attr' => ['class' => 'label-class'],
                ],
                'expanded' => true,
                'multiple' => false,
            ])
                ->add('submit','submit',[
                    'label'=>'Guardar'
                ])
            ->add('type_ticket', 'select', [
                'choices' => ['1' => 'General', '2' => 'VIP', '3' => 'PLATINIUM'],
                'rules' => 'required',
                'empty_value' => 'Selecciona tipo de entrada'
            ]);
//        dd($this);
    }
}
