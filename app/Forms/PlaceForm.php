<?php

namespace App\Forms;

use App\Models\Place;
use Kris\LaravelFormBuilder\Form;

class PlaceForm extends Form
{
//    protected $resource='place';

    public function buildForm()
    {
        if($this->getModel()&&$this->getModel()->id) {
            $method ='PUT';
            $url=route('place.update',$this->getModel()->id);
        }else{
            $method='POST';
            $url=route('place.store');
        }

        $this->formOptions=[
            'method'=>$method,
            'url'=>$url
        ];
        $this
            ->add('name', 'text',[
                'label'=>'Nombre',
                'rules'=>'required'
            ]);

        $this->add('submit','submit',[
            'label'=>'Guardar'
        ]);
}
}
