<?php

namespace App\Http\Controllers;

use App\Forms\PlaceForm;
use App\Models\Place;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct(FormBuilder $formBuilder){
        $this->formBuilder=$formBuilder;
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
//        dd($formBuilder);
        $form=$this->getForm();
//        $form = $formBuilder->create(\App\Forms\PlaceForm::class, [
//            'method' => 'POST',
//            'url' => route('place.store')
//        ]);

        return view('place.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FormBuilder $formBuilder)
    {
        $form = $this->getForm();
        $form->redirectIfNotValid();
        Place::create($form->getFieldValues());
//        $form->getModel()->save();
        return redirect()->route('place.index');
        // Do saving and other things...
    }
    private function getForm(?Place $place=null){
        $place=$place ?: new Place();
        return $this->formBuilder->create(PlaceForm::class,[
            'model'=>$place
        ]);
    }
    /**
     * Display the specified resource.
     */
    public function show(Place $place)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $place=Place::findOrFail($id);
       $form=$this->getForm($place);
//       dd($place);
       return view('place.edit',compact('form'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update( $id)
    {
        $place=Place::findOrFail($id);
        $form=$this->getForm($place);
        $form->redirectIfNotValid();
//        dd($place);
        $place->update($form->getFieldValues());
//        $place->save();
        return redirect()->route('place.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Place $place)
    {
        //
    }
}
