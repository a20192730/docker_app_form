<?php

namespace App\Http\Controllers;

use App\Forms\TourForm;
use App\Models\Tour;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct(FormBuilder $formBuilder){
        $this->formBuilder=$formBuilder;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(FormBuilder $formBuilder)
    {
        $form=$this->getForm();

        return view('tour.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FormBuilder $formBuilder)
    {

//        dd($form->getFieldValues());
        $form = $this->getForm();
        $form->redirectIfNotValid();
        $tour = Tour::create($form->getFieldValues());
        $tour->places()->attach($form->getFieldValues()['places'] ?? []);
        return redirect()->route('tour.index');
    }

    /**
     * Display the specified resource.
     */
    private function getForm(?Tour $tour=null){
        $tour=$tour ?: new Tour();
        return $this->formBuilder->create(TourForm::class,[
            'model'=>$tour
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $tour=Tour::findOrFail($id);
        $tour->places=$tour->places()->get()->pluck('id')->toArray();
        $form=$this->getForm($tour);
//        dd($form);
        return view('tour.edit',compact('form'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id)
    {
        $tour=Tour::findOrFail($id);
        $form=$this->getForm($tour);
        $form->redirectIfNotValid();
//        dd($place);
        $tour->update($form->getFieldValues());
        $tour->places()->detach();
        $tour->places()->attach($form->getFieldValues()['places'] ?? []);
        return redirect()->route('place.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tour $tour)
    {
        //
    }
}
