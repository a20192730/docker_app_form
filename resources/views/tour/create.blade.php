@extends('layouts.html_view')

@section('content')
    <div class="container">
        {!! form_start($form) !!}
        <div class="row">
            <div class="col-sm-6">
                {!! form_row($form->description) !!}
            </div>

            <div class="col-sm-6">
                {!! form_row($form->type_ticket) !!}
            </div>
            <div class="col-sm-6">
                {!! form_row($form->places) !!}
            </div>
            <div class="col-sm-6">
                {!! form_row($form->is_insurance) !!}
            </div>
        </div>
        {!! form_rest($form) !!}
        {!! form_end($form) !!}
        {{--        {!! form($form) !!}--}}

    </div>
@endsection





