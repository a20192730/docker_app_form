<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\TourController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
//*/
Route::get('/place/create', [PlaceController::class, 'create'])->name('place.create');
Route::get('/places', [PlaceController::class, 'index'])->name('place.index');
Route::post('/place/create', [PlaceController::class, 'store'])->name('place.store');
Route::get('/place/{id}/edit', [PlaceController::class, 'edit'])->name('place.edit');
Route::put('/place/{id}/update', [PlaceController::class, 'update'])->name('place.update');


Route::get('/tour/create', [TourController::class, 'create'])->name('tour.create');
Route::get('/tours', [TourController::class, 'index'])->name('tour.index');
Route::post('/tour/create', [TourController::class, 'store'])->name('tour.store');
Route::get('/tour/{id}/edit', [TourController::class, 'edit'])->name('tour.edit');
Route::put('/tour/{id}/update', [TourController::class, 'update'])->name('tour.update');

//Route::resource('place',PlaceController::class);

Route::get('/', function () {
    return view('welcome');
});
